<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';

    protected $fillable = [
        'reviews', 'koleksi_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function collection()
    {
        return $this->belongsTo('App\Models\Collection', 'koleksi_id');
    }
}

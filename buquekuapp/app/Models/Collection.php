<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{

    protected $table = 'collection';
    protected $guarded = [''];

    public function user()
    {
        return $this->belongsTo('App\User', 'pemilik_id');
    }

    public function review()
    {
        return $this->hasMany('App\Models\Review');
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Session;
use App\Models\Collection;
use App\Models\Review;
use DateTime;
use Alert;

class ReviewController extends Controller
{
    protected $task = '';

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $reviews = Review::with(['User', 'Collection'])->get();
        return view('review.index', compact('reviews'));
    }

    public function create()
    {
        if (Auth::check()) {
            $id = Auth::id();
            $collection = Collection::with('User')->where('pemilik_id', $id)->get();
            return view('review.create', compact('collection'));
        };
    }

    public function store(Request $request)
    {
        $this -> validate($request, [
            'reviews' => ['required', 'string'],
            'koleksi_id' => ['required'],
        ]);

        Review::create([
            'reviews' => $request->reviews,
            'koleksi_id' => $request->koleksi_id,
            'user_id' => Auth::id()
        ]);
        Alert::success(' Success Save ', 'Data Berhasil Ditambahkan!');
        return redirect(url('/review'))->with('status', 'Data Berhasil Ditambahkan!');
    }

    public function edit($id)
    {
        $userId = Auth::id();
        $collection = Collection::with('User')->where('pemilik_id', $userId)->get();

        $data = Review::where('id', $id)->first();
        return view('review.edit', compact('data', 'collection'));
    }

    public function update(Request $request, $id)
    {
        $this -> validate($request, [
            'reviews' => ['required', 'string'],
            'koleksi_id' => ['required'],
        ]);

        Review::where('id', $id)->update([
            'reviews' => $request->reviews,
            'koleksi_id' => $request->koleksi_id,
            'updated_at' => new DateTime()
        ]);
        Alert::success(' Success Update ', 'Data Berhasil Diperbaharui!');
        return redirect(url('/review'))->with('status', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        Review::where('id', $id)->delete();
        Alert::success(' Success Delete ', 'Data Berhasil dihapus!');
        return redirect(url('/review'))->with('status', 'Data berhasil dihapus');
    }

}

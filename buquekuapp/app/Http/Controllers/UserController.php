<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExport;
use DateTime;
use Alert;
use PDF;

class UserController extends Controller
{


    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        $title = 'Tambah User';
        return view('user.create', compact('title'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'photo' => ['string'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'photo' => $request->photo
        ]);
        Alert::success(' Success Save ', 'Data Berhasil Ditambahkan!');
        return redirect(url('/user'))->with('status', 'Data Berhasil Ditambahkan!');
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'photo' => ['string'],
        ]);

        User::where('id', $id)->update([
            'name' => $request->name,
            'photo' => $request->photo,
            'updated_at' => new DateTime()
        ]);
        Alert::success(' Success Update ', 'Data Berhasil Diperbaharui!');
        return redirect(url('/user'))->with('status', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        User::where('id', $id)->delete();
        Alert::success(' Success Delete ', 'Data Berhasil dihapus!');
        return redirect(url('/user'))->with('status', 'Data berhasil dihapus');
    }


    public function cetak_pdf()
    {
    	$users = User::all();

        $pdf = PDF::loadview('user.table_users_pdf', compact('users'));
        return $pdf->stream();
    }

    public function export_excel()
	{
		return Excel::download(new UserExport, 'users.xlsx');
	}

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Collection;
use Auth;
use DateTime;
use Alert;

class koleksicontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $koleksi     = Collection::all();
        return view('collection.index', compact('koleksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => ['required', 'string', 'max:255'],
            'pengarang' => ['required', 'max:255'],
            'image' => ['string'],
        ]);

        $koleksi = Collection::create([
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'image' => $request->image,
            'num_of_like' => 0,
            'num_of_dislike' => 0,
            'pemilik_id' => Auth::id()

        ]);
        Alert::success(' Success Save ', 'Data Berhasil Ditambahkan!');
        return redirect('/koleksi')->with('status', 'Data Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Collection::where('id', $id)->first();
        return view('collection.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required', 'string', 'max:255'],
            'pengarang' => ['required', 'max:255'],
            'image' => ['string'],
        ]);

        $koleksi = Collection::where('id', $id)->update([
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'image' => $request->image,
            'num_of_like' => 0,
            'num_of_dislike' => 0,
            'pemilik_id' => Auth::id(),
            'updated_at' => new DateTime()
        ]);
        Alert::success(' Success Update ', 'Data Berhasil Diperbaharui!');
        return redirect('/koleksi')->with('status', 'Data Berhasil Diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Collection::where('id', $id)->delete();
        Alert::success(' Success Delete ', 'Data Berhasil dihapus!');
        return redirect(url('/koleksi'))->with('status', 'Data berhasil dihapus');
    }
}

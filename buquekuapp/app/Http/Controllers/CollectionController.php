<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Collection;
use App\Models\Review;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CollectionExport;
use Alert;
use PDF;

class CollectionController extends Controller
{
    public function index()
    {
        $collection = Collection::with('User')->get();
        return view('welcome', compact('collection'));
    }

    public function detail($id)
    {
        $item = Collection::with('User')->where('id', $id)->first();
        $review = Review::where('koleksi_id', $id)->first();
        return view('collection.detail', compact('item', 'review'));
    }

    public function like($id) {
        $data = Collection::where('id', $id)->first();
        Collection::where('id', $id)->update([
            'num_of_like' => $data->num_of_like + 1,
        ]);
        Alert::success(' Like Review ', 'Anda menyukai review buku ini!');
        return redirect()->back();
    }

    public function dislike($id) {
        $data = Collection::where('id', $id)->first();
        Collection::where('id', $id)->update([
            'num_of_dislike' => $data->num_of_dislike + 1,
        ]);
        Alert::warning(' Dislike Review ', 'Anda tidak menyukai review buku ini!');
        return redirect()->back();
    }

    public function cetak_pdf()
    {
    	$collection = Collection::all();

    	$pdf = PDF::loadview('collection.table_koleksi_buku_pdf', compact('collection'));
        return $pdf->stream();
    }

    public function export_excel()
	{
		return Excel::download(new CollectionExport, 'koleksi-buku.xlsx');
	}
}

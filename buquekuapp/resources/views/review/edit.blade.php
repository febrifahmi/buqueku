@extends('layouts.app')
@section('content')
@unless (Auth::check())
You are not signed in.
@endunless
<div class="container">
    <div class="row">
        <div class="col">

        </div>
        <div class="col-8">
            <h3>Edit Review Buku</h3>
            <div class="card p-3">
                <form method="POST" action="{{url('/review') . '/' . $data->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="bukukoleksi">Pilih buku:</label><br>
                        <select id="bukukoleksi" name="koleksi_id" class="form-control @error('koleksi_id') is-invalid @enderror" value="{{$data->koleksi_id}}">
                            @forelse ($collection as $item)
                                <option value="{{ $item->id }}" {{ ($item->id == $data->koleksi_id) ? 'selected' : '' }}>{{ $item->judul }}</option>
                            @empty
                                <option value="" style="display: none">Anda belum punya koleksi buku</option>
                            @endforelse
                        </select>
                        @error('koleksi_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isireview">Review:</label><br>
                        <textarea  class="form-control @error('reviews') is-invalid @enderror" name="reviews" rows="10" cols="60" placeholder="Tulis review anda...">{{$data->reviews}}</textarea>
                        @error('reviews')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="col">

        </div>
    </div>
</div>

@endsection

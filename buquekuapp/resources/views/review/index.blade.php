@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('review/create') }}" class="btn btn-primary btn-sm mb-3">Buat Review</a>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Judul</th>
                                <th>Review</th>
                                <th>Pemilik</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reviews as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">{{$p->collection->judul}}</td>
                                <td><span class="d-inline-block text-truncate" style="max-width: 20rem;">{{$p->reviews}}</span></td>
                                <td>{{$p->user->name}}</td>
                                <td class="text-center">
                                    <a href="{{ url('/review') . '/' . $p->id . '/edit' }}" class="btn btn-xs btn-info">Edit</a>
                                    <form action="{{url('/review') . '/' . $p->id }}" method="post" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
                                </td>{{ ' ' }}
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <th>No</th>
                            <th class="text-center">Cover</th>
                            <th>Review</th>
                            <th>Pemilik</th>
                            <th class="text-center">Opsi</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>

@endsection

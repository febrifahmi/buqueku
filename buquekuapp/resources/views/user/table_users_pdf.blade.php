<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <table id="customers">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Photo</th>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $p)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td class="text-center">
                    @if($p->photo)
                        <img src="{{ $p->photo }}" class="rounded-circle" alt="image" style="width: 3rem; height: 3rem; object-fit: cover" />
                    @endif
                    </td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->email}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <th>No</th>
                <th class="text-center">Photo</th>
                <th>Name</th>
                <th>Email</th>
            </tfoot>
        </table>
    </div>
</body>
</html>

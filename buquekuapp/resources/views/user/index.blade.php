@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('user/create') }}" class="btn btn-primary btn-sm mb-3">Tambah User</a>
                    <a href="{{ url('users/cetak_pdf') }}" class="btn btn-danger btn-sm mb-3" target="_blank">Export to PDF</a>
                    <a href="{{ url('users/export_excel') }}" class="btn btn-success btn-sm mb-3" target="_blank">Export to EXCEL</a>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Photo</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">
                                @if($p->photo)
                                    <img src="{{ $p->photo }}" class="rounded-circle" alt="image" style="width: 3rem; height: 3rem; object-fit: cover" />
                                @endif
                                </td>
                                <td>{{$p->name}}</td>
                                <td>{{$p->email}}</td>
                                <td>
                                    <a href="{{ url('/user') . '/' . $p->id . '/edit' }}" class="btn btn-xs btn-info">Edit</a>
                                    @if (Auth::id() !== $p->id)
                                        <form action="{{url('/user') . '/' . $p->id }}" method="post" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                        </form>
                                    @endif
                                </td>{{ ' ' }}
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <th>No</th>
                            <th class="text-center">Photo</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{asset('assets/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Etc -->
    <script>
        $.widget.bridge("uibutton", $.ui.button);
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush

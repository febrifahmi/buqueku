@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 mt-3">
            <h3>Tambah User</h3>
            <div class="card p-3">
                <form method="post" action="{{ url('/user') }}">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="name" name="name">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="email" name="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="password" name="password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group ">
                        <label for="password-confirm">{{ __('Confirm Password') }}</label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="confirm password" required autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <label for="judul">Photo</label>
                        <input type="text" class="form-control @error('photo') is-invalid @enderror" id="photo" placeholder="https://source.unsplash.com/random" name="photo">

                        @error('photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

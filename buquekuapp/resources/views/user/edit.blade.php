@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 mt-3">
            <h3>Edit User</h3>
            <div class="card p-3">
                <form method="post" action="{{url('/user') . '/' . $user->id }}">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="judul">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="name" name="name" value="{{$user->name}}">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">Photo</label>
                        <input type="text" class="form-control @error('photo') is-invalid @enderror" id="photo" placeholder="https://" name="photo" value="{{ $user->photo }}">

                        @error('photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

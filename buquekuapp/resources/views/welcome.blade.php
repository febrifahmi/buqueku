@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <!-- @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif -->

        <div class="container grid-wrapper">

            @foreach ($collection as $item)
                <div class="card text-center" style="width: 100%;">
                    <img src="{{ $item->image }}" class="card-img-top" alt="image" style="width: 100%; height: 15rem; object-fit: cover" />
                    <div class="card-body">
                        <h5 class="card-title d-inline-block text-truncate" style="max-width: 15rem;">{{$item->judul}}</h5>
                        <ul class="list-group text-left">
                            <li class="list-group-item d-flex flex-row justify-content-start">
                                <span style="width: 5rem;">Likes </span>
                                <span>: {{$item->num_of_like}}</span>
                            </li>
                            <li class="list-group-item d-flex flex-row justify-content-start">
                                <span style="width: 5rem;">Author </span>
                                <span class="d-inline-block text-truncate" style="max-width: 6.5rem;">: {{$item->pengarang}}</span>
                            </li>
                            <li class="list-group-item d-flex flex-row justify-content-start">
                                <span style="width: 5rem;">Posting by </span>
                                <span class="d-inline-block text-truncate" style="max-width: 6.5rem;">: {{$item->user->name}}</span>
                            </li>
                        </ul><br />
                        <a href="{{ url('/detail') . '/' . $item->id }}" class="btn btn-primary">Lihat Review</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('koleksi/create') }}" class="btn btn-primary btn-sm mb-3">Tambah Koleksi Buku</a>
                    <a href="{{ url('collection/cetak_pdf') }}" class="btn btn-danger btn-sm mb-3" target="_blank">Export to PDF</a>
                    <a href="{{ url('collection/export_excel') }}" class="btn btn-success btn-sm mb-3" target="_blank">Export to EXCEL</a>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="text-center">Image</th>
                                <th>Judul</th>
                                <th>Pengarang</th>
                                <th>Like</th>
                                <th>Dislike</th>
                                <th>Pemilik</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($koleksi as $p)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center"><img src="{{ $p->image }}" class="img-thumbnail" alt="image" style="width: 4rem; height: 5rem; object-fit: cover" /></td>
                                <td>{{$p->judul}}</td>
                                <td>{{$p->pengarang}}</td>
                                <td>{{$p->num_of_like}}</td>
                                <td>{{$p->num_of_dislike}}</td>
                                <td>{{$p->user->name}}</td>
                                <td class="text-center">
                                    <a href="{{ url('/koleksi') . '/' . $p->id  . '/edit' }}" class="btn btn-xs btn-info">Edit</a>
                                    <form action="{{url('/koleksi') . '/' . $p->id }}" method="post" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <th>No</th>
                            <th class="text-center">Image</th>
                            <th>Judul</th>
                            <th>Pengarang</th>
                            <th>Like</th>
                            <th>Dislike</th>
                            <th>Pemilik</th>
                            <th class="text-center">Opsi</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection

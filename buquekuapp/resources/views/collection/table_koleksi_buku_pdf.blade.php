<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
        }
        </style>
</head>
<body>
    <div class="container-fluid">
        <table id="customers">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Image</th>
                    <th>Judul</th>
                    <th>Pengarang</th>
                    <th>Like</th>
                    <th>Dislike</th>
                    <th>Pemilik</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($collection as $p)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td class="text-center"><img src="{{ $p->image }}" class="img-thumbnail" alt="image" style="width: 4rem; height: 5rem; object-fit: cover" /></td>
                    <td>{{$p->judul}}</td>
                    <td>{{$p->pengarang}}</td>
                    <td>{{$p->num_of_like}}</td>
                    <td>{{$p->num_of_dislike}}</td>
                    <td>{{$p->user->name}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <th>No</th>
                <th class="text-center">Image</th>
                <th>Judul</th>
                <th>Pengarang</th>
                <th>Like</th>
                <th>Dislike</th>
                <th>Pemilik</th>
            </tfoot>
        </table>
    </div>
</body>
</html>

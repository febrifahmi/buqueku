<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CollectionController@index');

Route::get('/detail/{collection_id}', 'CollectionController@detail');

Route::get('/like/{collection_id}', 'CollectionController@like');
Route::get('/dislike/{collection_id}', 'CollectionController@dislike');

Route::get('/collection/cetak_pdf', 'CollectionController@cetak_pdf');
Route::get('/users/cetak_pdf', 'UserController@cetak_pdf');

Route::get('/collection/export_excel', 'CollectionController@export_excel');
Route::get('/users/export_excel', 'UserController@export_excel');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('review', 'ReviewController')->middleware('auth');

Route::resource('user', 'UserController')->middleware('auth');
Route::resource('koleksi', 'koleksicontroller')->middleware('auth');

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');

        // membuat data dummy sebanyak 10 record
        for($x = 1; $x <= 2; $x++){

        	// insert data dummy pegawai dengan faker
        	DB::table('collection')->insert([
                'judul' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'pengarang' => $faker->name,
                'num_of_like' => 0,
                'num_of_dislike' => 0,
                'image' => 'https://source.unsplash.com/random',
                'pemilik_id' => 1,
            ]);

        }

    }
}

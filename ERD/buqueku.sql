-- MySQL Script generated by MySQL Workbench
-- Thu Nov 26 20:30:19 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema koleksibuku
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema koleksibuku
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `koleksibuku` DEFAULT CHARACTER SET utf8 ;
USE `koleksibuku` ;

-- -----------------------------------------------------
-- Table `koleksibuku`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koleksibuku`.`user` (
  `iduser` INT NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(100) NULL,
  `email` VARCHAR(100) NULL,
  `password` VARCHAR(100) NULL,
  `profpic` VARCHAR(255) NULL,
  PRIMARY KEY (`iduser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `koleksibuku`.`reviewbuku`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koleksibuku`.`reviewbuku` (
  `idreviewbuku` INT NOT NULL AUTO_INCREMENT,
  `review` TEXT(10000) NULL,
  `imagebuku` VARCHAR(100) NULL,
  PRIMARY KEY (`idreviewbuku`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `koleksibuku`.`koleksibuku`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koleksibuku`.`koleksibuku` (
  `idbuku` INT NOT NULL AUTO_INCREMENT,
  `judul` TEXT(1000) NULL,
  `pengarang` VARCHAR(255) NULL,
  `num_of_like` INT NULL,
  `num_of_dislike` INT NULL,
  `pemilik_id` INT NULL,
  `review_id` INT NULL,
  PRIMARY KEY (`idbuku`),
  INDEX `pemilik_id_idx` (`pemilik_id` ASC),
  INDEX `review_id_idx` (`review_id` ASC),
  CONSTRAINT `pemilik_id`
    FOREIGN KEY (`pemilik_id`)
    REFERENCES `koleksibuku`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `review_id`
    FOREIGN KEY (`review_id`)
    REFERENCES `koleksibuku`.`reviewbuku` (`idreviewbuku`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `koleksibuku`.`komentar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koleksibuku`.`komentar` (
  `idkomentar` INT NOT NULL,
  `komentar` TEXT(5000) NULL,
  `user_id` INT NULL,
  `review_id` INT NULL,
  PRIMARY KEY (`idkomentar`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `review_id_idx` (`review_id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `koleksibuku`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `review_id`
    FOREIGN KEY (`review_id`)
    REFERENCES `koleksibuku`.`reviewbuku` (`idreviewbuku`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
